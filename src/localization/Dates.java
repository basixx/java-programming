package localization;

import java.time.DateTimeException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

public class Dates {
    
    static void oldToNew() {
        System.out.println("----Old to new---------");
        Date oldDate = new Date();
        LocalDate date = oldDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        System.out.println("Date v1: "+date);
        LocalDate localDate = oldDate.toInstant().atZone(ZoneId.of("EET")).toLocalDate();
        System.out.println("Date v2: " + localDate);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(oldDate.toInstant(), ZoneId.of("Mexico/General"));
        System.out.println("Time v1: " + localDateTime);
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(oldDate.toInstant(), ZoneId.of("Mexico/General"));
        System.out.println("Time v2: " + zonedDateTime);
        localDateTime = LocalDateTime.ofInstant(oldDate.toInstant(), ZoneId.of("+0300"));
        System.out.println("Time v3: " + localDateTime);
        OffsetDateTime ofsDT=OffsetDateTime.of(localDateTime, ZoneOffset.of("-2"));
        System.out.println("Offset datetime: "+ofsDT);
        
        Date newDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        System.out.println("New date:"+newDate);
    }
    
    static public void calculations(){
        LocalDate currentDate = LocalDate.now();
        System.out.println("LocalDate.now(): " + currentDate);

        LocalTime currentTime = LocalTime.now();
        System.out.println("LocalTime.now(): " + currentTime);

        LocalDateTime currentDateTime = LocalDateTime.now();
        System.out.println("LocalDateTime.now(): " + currentDateTime);

        Duration dur = Duration.ofDays(35);
        System.out.println("Duration.ofDays(35): " + dur);
        System.out.println("\t(35*24 = " + 35 * 24 + ")");

        LocalTime time = LocalTime.now();
        LocalTime newTime = time.plus(2, ChronoUnit.HOURS);
        System.out.println("LocalTime.now() + 2 hours: " + newTime);
        Period p = Period.ofDays(2);
        System.out.println("LocalDate.now() + 2 days: " + LocalDate.now().plus(p));

        // Just a sample
        LocalDate newLocalDate = currentDate.atStartOfDay().plus(dur).toLocalDate();
        System.out.println("Now start of day + 35 days: " + newLocalDate);
        newLocalDate = currentDate.plus(Period.ofDays(35));
        System.out.println("Now + 35 days: " + newLocalDate);

        LocalDate xmas = currentDate.withMonth(Month.DECEMBER.ordinal() + 1).withDayOfMonth(25);
        // Or
        // LocalDate xmas = currentDate.withMonth(Month.DECEMBER.getValue()).withDayOfMonth(25);
        System.out.println(xmas);

        long days = currentDate.until(xmas, ChronoUnit.DAYS);
        System.out.println(days + " until christmas");

        LocalDate newYear = LocalDate.of(currentDate.getYear() + 1, Month.JANUARY, 1);
        System.out.println(newYear);

        days = currentDate.until(newYear, ChronoUnit.DAYS);
        System.out.println(days + " days until new year");

        LocalDateTime xmasDateTime = LocalDateTime.of(xmas, LocalTime.MIDNIGHT);
        DateTimeFormatter isoFormatter = DateTimeFormatter.ISO_DATE_TIME;
        DateTimeFormatter rfcFormatter = DateTimeFormatter.RFC_1123_DATE_TIME;
        DateTimeFormatter localFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        System.out.println("XMas (ISO): " + xmasDateTime.format(isoFormatter));
        System.out.println("XMas (DEF locale): " + xmas.format(localFormatter));
        System.out.println("XMas (FRENCH): " + xmas.format(localFormatter.withLocale(Locale.FRANCE)));
        try {
            System.out.println("Koitetaan RFC...");
            System.out.println("XMas (RFC): " + xmasDateTime.format(rfcFormatter));
        } catch (UnsupportedTemporalTypeException ex) {
            System.out.println(ex.getMessage());
        }
        ZonedDateTime zonedXmas = xmasDateTime.atZone(ZoneId.systemDefault());
        System.out.println("XMas (RFC): " + zonedXmas.format(rfcFormatter));
        
    }
 
    static void timezoneSamples() {
        System.out.println("Short zones:");
        for (Map.Entry<String, String> entry : ZoneId.SHORT_IDS.entrySet()) {
            System.out.printf("\t%s : %s\n", entry.getKey(), entry.getValue());
        }

        System.out.println("European zones");
        LocalDateTime dateTime = LocalDateTime.now();
        ZoneId.getAvailableZoneIds().stream().
                map(s -> ZoneId.of(s))
                .filter(zone -> (zone.getId().contains("Europe")))
                .forEach(zone -> System.out.printf("\t%s(%s): %s\n",
                                zone.getId(),
                                zone.getDisplayName(TextStyle.FULL, Locale.ENGLISH),
                                dateTime.atZone(zone).getOffset().getId())
                );

        System.out.println("** Samples");
        String z = ZoneId.of("+0200").getDisplayName(TextStyle.FULL, Locale.ENGLISH);
        System.out.println(z);
        System.out.println(ZoneId.of("Europe/Helsinki").getDisplayName(TextStyle.FULL, Locale.ENGLISH));
        System.out.println(ZoneId.of("Europe/Helsinki").getDisplayName(TextStyle.FULL, new Locale("FI")));
        System.out.println(ZoneId.of("GMT").getDisplayName(TextStyle.FULL, Locale.ENGLISH));
        System.out.println(ZoneId.of("UTC").getDisplayName(TextStyle.FULL, Locale.ENGLISH));
        System.out.println(ZoneId.of("UTC").getDisplayName(TextStyle.FULL, Locale.FRENCH));
    }

    static void otherSamples() {
        System.out.println("Weekdays");
        Arrays.asList(DayOfWeek.values())
                .stream()
                //.map(dow -> dow.getDisplayName(TextStyle.FULL, new Locale("FI")))
                .map(dow -> dow.getDisplayName(TextStyle.SHORT, Locale.ENGLISH))
                .forEach(System.out::println);

        String viikonpaiva = LocalDate.now()
                .with(TemporalAdjusters.firstDayOfMonth())
                //.with(TemporalAdjusters.lastDayOfYear())
                .getDayOfWeek()
                //.name()
                .getDisplayName(TextStyle.FULL, new Locale("FI"));
        System.out.println("1st day of the month was: " + viikonpaiva);

        try {
            LocalDate hassu = LocalDate.of(2015, Month.FEBRUARY, 29);
            System.out.println("Funny: " + hassu.toString());
        } catch (DateTimeException ex) {
            System.out.println("Error: " + ex.getLocalizedMessage());
        }

    }
    
    static public void main(String[] args){
        oldToNew();
        calculations();
        timezoneSamples();
        otherSamples();
    }
    
}
