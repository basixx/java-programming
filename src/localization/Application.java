package localization;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;


/*
    Create Messages resource-bundle (
    Messages.properties,
    Messages_fi.properties
    and Messages_en.properties) to your source root.

    Add the following variables (with different values) to your properties files:
    - greeting=Hello  
    - dateFormat=MMM dd
    - topic=important issues
    - message={1}, I will meet you at {0} to discuss {2}

*/

public class Application {

    static void test(Locale loc){
        Locale.setDefault(loc);
        ResourceBundle b=ResourceBundle.getBundle(
                "localization.Messages");
        String s=b.getString("message");
        Object[] pars={
            b.getString("greeting"),
            new Date(),
            b.getString("topic")
        };
        MessageFormat msg=new MessageFormat(s);
        msg.setFormatByArgumentIndex(1, new SimpleDateFormat(b.getString("dateFormat")));

        System.out.println(msg.format(pars));
        
        
        double number=-123456.789;
        NumberFormat nf=NumberFormat.getNumberInstance();
        NumberFormat cf=NumberFormat.getCurrencyInstance();
        System.out.println("Numero "+nf.format(number));
        System.out.println("Valuutta "+cf.format(number));
        
        // LocalDate dt= Pyydetään käyttäjältä
        LocalDate currentDate=LocalDate.now();
        DateTimeFormatter formatter=DateTimeFormatter
                .ofLocalizedDate(FormatStyle.SHORT);
        String tx=currentDate.format(formatter);
        System.out.println(tx);
        System.out.print("Anna pvm:");
        Scanner sc=new Scanner(System.in);
        tx=sc.nextLine();
        LocalDate parsed=LocalDate.parse(tx,formatter);
        System.out.println(parsed);
        
    }
    
    static public void main(String[] args){
        test(new Locale("fi"));
        test(Locale.ENGLISH);
        /*
        Read the information from the bundle with different locales.
        Use MessageFormat to place greeting, topic and current date to
        message. Use dateFormat value to format the date
    
        Also try to print numbers and currencies formatted for different locales.
        
        Work with different ways to use dates. You might want to go through older
        classes Date and Calendar, but especially work with LocalDate and DateFormatter.
        
        */
    }
    
}
