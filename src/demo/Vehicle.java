/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author jyrki
 */
abstract public class Vehicle {
    protected int speed=0;

    abstract public void setSpeed(int speed);

    public int accelerate(int ds){
        System.out.println("kiihtytellään");
        setSpeed(speed+ds);
        return speed;
    }
    
    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    
}
