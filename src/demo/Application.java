/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import java.util.Date;

/**
 *
 * @author jyrki
 */
public class Application {
    
    static <T1,T2> String yhdista(T1 a, T2 b){
        return a.toString()+":"+b.toString();
    }
    
    static public void main(String[] args){
        
        System.out.println(yhdista("Päiväys",new Date()));
        
        ObjectPair op=new ObjectPair("Terve",new Date());
        Date d=(Date)op.getB();
        System.out.println(op);
        op.setB("Moikka");
        System.out.println(op);
        
        Pair<String,Date> p=new Pair("Terve",new Date());
        Date d2=p.getB();
        System.out.println(p);
        //p.setB("Moi");
        
        Pair<String,String> ps=new Pair("Terve","Maailma");
        System.out.println(ps);
        Pair<String,Car> psc=new Pair("AKU-313",new Car(45));
        System.out.println(psc);
    }
    
    static public boolean isNumber(String n){
        try{
            Integer.parseInt(n);
            return true;
        }
        catch(Exception ex){
            return false;
        }
    }
    
    static public void laske(String lasku) throws LaskentaPoikkeus{
        String[] opers=lasku.split(" ");
        try{
            int l1=Integer.parseInt(opers[0]);
            int l2=Integer.parseInt(opers[2]);
            if ((l1>10) || (l2>10)) throw new LaskentaPoikkeus(l1,l2);
            System.out.println(l1+l2);
        }
        catch(ArrayIndexOutOfBoundsException aex){
            System.out.println("Virheellinen laskutoimitus");
        }
        catch(NumberFormatException nex){
            System.out.println("Virheellinen operandi");
        }
    }

    static public void operoi(Motorized m){
        m.start();
        System.out.println("Operoidaan");
        m.use();
        m.stop();
    }
    
    static public void exmain(String[] args){
        try{
            laske("1 + 30");
        }
        catch(LaskentaPoikkeus lp){
            System.out.println(lp.getMessage()+", "+lp.luku1+" + "+lp.luku2);
        }
        
        Car c=new Car(50);
        operoi(c);
        
        Lawnmower l=new Lawnmower();
        operoi(l);
    }
    
    static public void kiihdyttele(Vehicle c){
        c.accelerate(20);
    }
    
    static public void carmain(String[] args){
        
        Car c1=new Car();
        Bus c2=new Bus(45,3);
        Bus c3=new Bus(70);
        System.out.println(c3 instanceof Car);
        kiihdyttele(c1);
        kiihdyttele(c3);
        //c3.accelerate(30);
        System.out.println(c3);
        System.out.println(c1);
        System.out.println("Nopeus "+c1.getSpeed()+", "+c2.getSpeed());
        System.out.println(c1==c2);
        System.out.println(c1.equals(c2));
    }
}
