/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author jyrki
 */
class ObjectPair{
    private Object a,b;
    
    ObjectPair(Object a, Object b){
        this.a=a;
        this.b=b;
    }
    
    public String toString(){
        return "("+getA()+","+getB()+")";
    }

    /**
     * @return the a
     */
    public Object getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(Object a) {
        this.a = a;
    }

    /**
     * @return the b
     */
    public Object getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(Object b) {
        this.b = b;
    }
}

public class Pair<T1,T2> {
    private T1 a;
    private T2 b;
    
    public Pair(T1 a,T2 b){
        this.a=a;
        this.b=b;
    }
    
    public String toString(){
        return "("+getA()+","+getB()+")";
    }

    /**
     * @return the a
     */
    public T1 getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(T1 a) {
        this.a = a;
    }

    /**
     * @return the b
     */
    public T2 getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(T2 b) {
        this.b = b;
    }
}
