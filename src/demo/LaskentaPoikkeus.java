/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author jyrki
 */
public class LaskentaPoikkeus extends Exception{
    public int luku1,luku2;
    
    public LaskentaPoikkeus(int l1,int l2){
        super("Laskenta ei onnistu");
        luku1=l1;
        luku2=l2;
    }
    
}
