/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author jyrki
 */
public class Pixel extends Pair<Integer,Integer> {
    String color;
    
    public Pixel(int x,int y,String color){
        super(x,y);
        this.color=color;
    }
    
}
