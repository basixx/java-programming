/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author jyrki
 */
public class Car extends Vehicle implements Cloneable,Motorized{
   public static int renkaita=4;
   
   public Car(){
       this(0);
   }
   
    public Car(int s){
        setSpeed(s);
    }
/*
    public int accelerate(int ds){
        this.setSpeed(this.speed+ds);
        return this.speed;
    }
*/

    public Car clone(){
        try{
            return (Car)super.clone();
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public void setSpeed(int speed) {
        if ((speed<-20) || (speed>120)) return;
        this.speed = speed;
    }
    
    public void tulosta(){
        System.out.println("Auton nopeus "+speed+", renkaita "+renkaita);
    }
    
    @Override
    public String toString(){
        return "Auto, nopeus "+speed;
    }
    
    public int hashCode(){
        return speed;
    }
    
    public boolean equals(Object o){
        if (o==null) return false;
        if (!(o instanceof Car)) return false;
        Car c=(Car)o;
        if (this.speed!=c.speed) return false;
        return true;
    }
    
    public void finalize(){
        System.out.println("Auto katoaa");
    }

    @Override
    public void start() {
        System.out.println("Broom");
    }

    @Override
    public void stop() {
        System.out.println("Sammuu");
    }

    @Override
    public void use() {
        System.out.println("Auto liikkuu");
    }
}
