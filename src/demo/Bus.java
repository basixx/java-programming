/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author jyrki
 */
public class Bus extends Car{
    private int numPsg=0;

    public Bus(int s){
        super(s);
        setNumPsg(0);
    }
    
    public Bus(int s, int p){
        super(s);
        setNumPsg(p);
    }
    
    @Override
    public int accelerate(int ds){
        System.out.println("Bussi kiihtyy hitaasti");
        return super.accelerate(ds);
    }
    
    public void takePassangers(int dp){
        setSpeed(0);
        setNumPsg(numPsg+dp);
    }
    
    public String toString(){
        String data=super.toString();
        data+=", matkustajia "+numPsg;
        return data;
    }
    
    /**
     * @return the numPsg
     */
    public int getNumPsg() {
        return numPsg;
    }

    /**
     * @param numPsg the numPsg to set
     */
    public void setNumPsg(int numPsg) {
        this.numPsg = numPsg;
    }
    
    
}
