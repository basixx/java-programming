package jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 1.  First we need to create the database using DBCreator -application.
        Follow your instructors lead on that

    2.  Make the connection to the database in the main. Run a simple query
        "SELECT * FROM Person", iterate the resultset and display information
        from each row.

    3.  Implement Person-class as JavaBean holding fields that match to columns
        of Person-table in the database. Implement PersonDAO-class:
        - List&lt;Person&gt; getAll()
        - Person get(int id)
        - void create(Person p)
        - void update(Person p)
        - void delete(int id)
        Each method should open (and close) the connection. You might want to
        implement a helper to PersonDAO
        - Connection getConnection()
        Also you will be populating Person-objects from a resultset, following 
        helper might come in handy
        - Person fromResultSet(ResultSet rs)
        
*/
public class Application {
    
    static public void listaa(String title){
        System.out.println(title+"__________________");
        PersonDAO pdao=new PersonDAO();
        List<Person> pl=pdao.getAll();
        for(Person p:pl)
            System.out.println(p);
    }
    
    
    static public void main(String[] args){
        PersonDAO pdao=new PersonDAO();
        listaa("Alku");
        
        Person p=pdao.get(1);
        p.setName("John Doe");
        pdao.update(p);
        listaa("Päivityksen jälkeen");
        
        Person p2=new Person();
        p2.setCompanyId(2);
        p2.setName("Andy Mccoy");
        p2.setEmail("andy@mccoy.net");
        p2.setContactDate(new Date(116,6,2));
        pdao.create(p2);
        System.out.println("Uuden id "+p2.getId());
        listaa("Lisäyksen jälkeen");
        pdao.delete(3);
        listaa("Poiston jälkeen");
    }
    
    
    static public void xmain(String[] args){
        try{
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            Connection con=DriverManager.getConnection(
                    "jdbc:derby://localhost:1527/crm","APP","app");
            PreparedStatement stm=con.prepareStatement(
                    "SELECT * FROM Person where name like ?",
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_UPDATABLE);
            stm.setString(1,"%Mo%");
            ResultSet rs=stm.executeQuery();
   
            while(rs.next()){
                System.out.println(rs.getString("name")+
                            ","+rs.getString("email")+
                            ","+rs.getDate("contact_date"));
                if (rs.getDate("contact_date").getYear()<113){
                    rs.updateDate("contact_date",new Date(117,3,4));
                    rs.updateRow();
                }
            }
            
            rs.close();
            stm.close();
            con.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
