/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jyrki
 */
public class PersonDAO {

    private Connection getConnection(){
        try{
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            return DriverManager.getConnection(
                    "jdbc:derby://localhost:1527/crm","APP","app");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    
    public List<Person> getAll(){
        List<Person> pl=new ArrayList();
        try(Connection con=getConnection()){
            Statement stm=con.createStatement();
            ResultSet rs=stm.executeQuery("SELECT * FROM Person");
            while(rs.next()){
                Person p=Person.fromResultSet(rs);
                pl.add(p);
            }
            rs.close();
            stm.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return pl;
    }
    
    public Person get(int id){
        Person p=null;
        try(Connection con=getConnection()){
            PreparedStatement stm=con.prepareStatement(
                    "SELECT * FROM Person where id=?");
            stm.setInt(1, id);
            ResultSet rs=stm.executeQuery();
            if(rs.next()){
                p=Person.fromResultSet(rs);
            }
            rs.close();
            stm.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return p;
    }
    
    public void update(Person p){
        try(Connection con=getConnection()){
            PreparedStatement stm=con.prepareStatement(
                    "SELECT * FROM Person where id=?",
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_UPDATABLE);
            stm.setInt(1, p.getId());
            ResultSet rs=stm.executeQuery();
            if(rs.next()){
                rs.updateInt("company_id", p.getCompanyId());
                rs.updateString("name",p.getName());
                rs.updateString("email",p.getEmail());
                rs.updateDate("contact_date", p.getContactDate());
                rs.updateRow();
            }
            rs.close();
            stm.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    public void delete(int id){
        try(Connection con=getConnection()){
            PreparedStatement stm=con.prepareStatement(
                    "DELETE FROM Person where id=?");
            stm.setInt(1, id);
            stm.executeUpdate();
            stm.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void create(Person p){
        String q="INSERT INTO Person";
        q+="(company_id,name,email,contact_date)";
        q+="values(?,?,?,?)";
        try(Connection con=getConnection()){
            PreparedStatement stm=con.prepareStatement(q,
                    Statement.RETURN_GENERATED_KEYS);
            stm.setInt(1, p.getCompanyId());
            stm.setString(2,p.getName());
            stm.setString(3, p.getEmail());
            stm.setDate(4,p.getContactDate());
            stm.executeUpdate();
            ResultSet rs=stm.getGeneratedKeys();
            if (rs!=null){
                if (rs.next()){
                    p.setId(rs.getInt(1));
                }
                rs.close();
            }
            stm.close();
            con.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }    
}
