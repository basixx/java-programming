/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Date;
import java.sql.ResultSet;

public class Person {
    private int id;
    private int companyId;
    private String name;
    private String email;
    private Date contactDate;

    static Person fromResultSet(ResultSet rs){
        Person p=new Person();
        try{
            p.setId(rs.getInt("id"));
            p.setCompanyId(rs.getInt("company_id"));
            p.setName(rs.getString("name"));
            p.setEmail(rs.getString("email"));
            p.setContactDate(rs.getDate("contact_date"));
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return p;
    }
    
    public String toString(){
        return id+","+companyId+","+name+","+email+","+contactDate;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the contactDate
     */
    public Date getContactDate() {
        return contactDate;
    }

    /**
     * @param contactDate the contactDate to set
     */
    public void setContactDate(Date contactDate) {
        this.contactDate = contactDate;
    }
    
}
