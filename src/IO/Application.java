package IO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {
    static String prompt(String txt){
        System.out.print(txt);
        try{
            BufferedReader rd=new BufferedReader(
                    new InputStreamReader(System.in));
            return rd.readLine();
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        return "";
    }

    static public void ex1(String[] args){
        //String s=prompt("Anna teksti:");
        //System.out.println(s);
        //FileHandler.create("c:/a-coursedata/test.txt");
        try(FileHandler fh=new FileHandler("c:/a-coursedata/test.txt")){
            while(true){
                String s=fh.readLine();
                if (s.length()==0) break;
                System.out.println(s);
            }
        }
        System.gc();
    }
    
    static public void main(String[] args){
        Person p=new Person("Tim",14);
        p.save("c:/a-coursedata/person.dat");
        p.saveXML("c:/a-coursedata/person.xml");
        
        Person p2=Person.read("c:/a-coursedata/person.dat");
        Person p3=Person.readXML("c:/a-coursedata/person.xml");
        System.out.println(p2.getName());
        System.out.println(p3.getName());
        /* 
            Modify person class to make the following work through serialization
            Person p=new Person("Tim",14);
            p.save("c:/course/person.dat");
            Person p2=Person.read("c:/course/person.dat");
            System.out.println(p2.getName());
        
            Also work with JAXB-serialization
            p.saveXML("c:/course/person.xml");
            Person p3=Person.readXML("c:/course/person.xml");
            System.out.println(p3.getName());
        */
        
    }
}
