/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jyrki
 */
public class FileHandler implements AutoCloseable{
    BufferedReader reader=null;
    
    public FileHandler(String fn){
        try {
            reader=new BufferedReader(new FileReader(fn));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    public String readLine(){
        try {
            String s = reader.readLine();
            if (s==null) s="";
            return s ;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "";
    }
    
    @Override
    public void close(){
        try{
            System.out.println("Closing");
            if (reader!=null) reader.close();
            reader=null;
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }
    
    protected void finalize(){
        this.close();
    }
    
    static public void create(String fn){
        try(PrintWriter pw=new PrintWriter(fn)){
            while(true){
               String s=Application.prompt("Anna dataa:");
               if (s.equals("x")) break;
               pw.println(s);
            }
            pw.flush();
            //pw.close();
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }
}
