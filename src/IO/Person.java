package IO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder={"name","age"})
public class Person implements Serializable {
    private String name="No name";
    private int age=0;

    public Person(){
    }
    
    public Person(String n, int a){
        setName(n);
        setAge(a);
    }
    
    /**
     * @return the name
     */
    @XmlAttribute(name="personName")
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        if (name==null) return;
        if (name.length()==0) return;
        this.name = name;
    }

    /**
     * @return the age
     */
    //@XmlAttribute
    @XmlAttribute
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        if ((age>=0) && (age<100)) this.age = age;
    }
    
    public void save(String fn){
        try(ObjectOutputStream oos=new ObjectOutputStream(
                new FileOutputStream(fn))){
            oos.writeObject(this);
            oos.flush();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    static public Person read(String fn){
        try(ObjectInputStream ois=new ObjectInputStream(
                new FileInputStream(fn))){
            Person p=(Person)ois.readObject();
            return p;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    
    public void saveXML(String fn){
        try{
            JAXBContext ctx=JAXBContext.newInstance(Person.class);
            Marshaller m=ctx.createMarshaller();
            m.marshal(this, new File(fn));
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    static public Person readXML(String fn){
        try{
            JAXBContext ctx=JAXBContext.newInstance(Person.class);
            Unmarshaller m=ctx.createUnmarshaller();
            Person p=(Person)m.unmarshal(new File(fn));
            return p;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
