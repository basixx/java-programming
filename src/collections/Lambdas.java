package collections;

/*
    Implement Calculate-interface with total-method taking two double-parameters
    and returning a double value.
*/
@FunctionalInterface
interface Calculate{
    double total(double a, double b);
    
    default double multi(double times,double a,double b){
        return times*total(a,b);
    }
}


public class Lambdas {

    /*
        Implement showVatPrice that takes net-price (double), vat (double) and
        Calculate-object as parameter. It uses Calculate.total to get the total
        price, then calculates vatAmount=total-net And displays all three values
    */
    
    static void showVatPrice(double net, double vat,Calculate c){
        double total=c.total(net, vat);
        double vatAmount=total-net;
        System.out.println("Net "+net+", vat "+vatAmount+", total "+total);
    }
    
    static public double sum(double a,double b){
        return a+b;
    }
    
    static public void main(String[] args){
        showVatPrice(100,24,new Calculate(){
            public double total(double a,double b){
                return a+b;
            }
        });
        showVatPrice(100,24,(a,b) -> a+b);
        showVatPrice(200,24,(a,b) -> a*(100+b)/100);
        showVatPrice(300,0.24,(a,b) -> a*(1+b));
        showVatPrice(50,12,Lambdas::sum);
        
        Calculate c = (a,b) -> a-b;
        System.out.println(c.multi(5,4,2));
        /*
            Call showVatPrice first with an instance of anonymous class, then 
            using lambdas. Pass vat in different ways
            - Absolute euro amount
            - Percentage-value 24
            - Percentage-value 0.24
            And provide correct algorigthm for total in each case
        
            Create sum-method to this class. Use method-reference to 
            use it as algorithm for total.
        
            Add a default method to the interface, for example 
            - double mult(int times, double a, double b)
        
            Test by:
            Calculate c=(a,b) -> a+b;
            System.out.println("Total "+c.times(4,2.1,3.2);
        */
    }
}
