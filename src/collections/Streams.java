package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {
    static public void mainx(String[] args){
        String[] weekdays={"monday","tuesday","wednesday",
                    "thursday","friday","saturday","sunday",
                     "Monday"};
        Stream.of(weekdays)
                .filter(s -> !s.toUpperCase().contains("T"))
                .sorted((a,b) -> a.length()-b.length())
                .forEach(System.out::println);
        
        String s=Stream.of(weekdays)
                .filter(s1 -> s1.contains("u"))
                .collect(Collectors.joining("-"));
        System.out.println(s);
        
        int ml=Stream.of(weekdays)
                .reduce("", (a,b) -> a.length() > b.length() ? a : b)
                .length();
        System.out.println("Maxlen 1:"+ml);
        
        ml=Stream.of(weekdays)
                .max((a,b) -> a.length() - b.length())
                .orElse("")
                .length();
        System.out.println("Maxlen 2:"+ml);
        
        ml=Stream.of(weekdays)
                .map(String::length)
                .max((a,b) -> a-b)
                .orElse(0);
        System.out.println("Maxlen 3:"+ml);

        List<String> wdl=Stream.of(weekdays).collect(Collectors.toList());
        List<String> tl=wdl.stream()
                    .filter(s1 -> s1.contains("t"))
                    .collect(Collectors.toList());
        wdl.removeAll(tl);
        wdl.forEach(System.out::println);
        
        List<Person> pl=new ArrayList();
        pl.add(new Person("Bart",14));
        pl.add(new Person("Lisa",10));
        pl.add(new Person("Marge",38));
        pl.add(new Person("Homer",41));
        pl.stream()
                .filter(p -> p.getAge()>=18)
                .sorted((p1,p2) -> p1.getName().compareTo(p2.getName()))
                .forEach(p -> System.out.println(p.getName()));
        System.out.println("Optional");
        Optional<Person> op=pl.stream()
                .parallel()
                .filter(p -> p.getAge()>=58)
                .sorted((p1,p2) -> p1.getName().compareTo(p2.getName()))
                .findFirst();
        System.out.println(op.orElse(new Person("John Doe",81)).getName());
        //op.ifPresent(p -> System.out.println(p.getName()));
        /*
        if (op.isPresent()){
            System.out.println(op.get().getName());
        }
        else System.out.println("Not found");
        */
    }
    
    static public void ex2(String[] args){
        /*
            Create a List<Person> with some persons.
            
            Print all adults (age>=18) in order by their name.
            Find the first adult and print his name
            
            Add a printout to Person.getAge() where you display 
            the Thread.currentThread().hashCode(). Use the streams
            in parallel manner.  You should see that ages are queried
            in different threads.
        */
    }
    
    static public void main(String[] args){
        List<Integer> li1=IntStream.range(1, 1000)
                .map(a -> (int)(Math.random()*100))
                .boxed()
                .collect(Collectors.toList());
        
        List<Integer> li2=IntStream
                .generate(() -> (int)(Math.random()*100))
                .limit(1000)
                .boxed()
                .collect(Collectors.toList());

        int sum=li1.stream().reduce(0, (a,b) -> a+b);
        
        System.out.println("Summa "+sum+", average "+sum/li1.size());
        
        Map<Integer,Integer> m=li1.stream()
                .distinct()
                .collect(Collectors.toMap(
                        k -> k, 
                        k -> (int)li1.stream().filter(v -> v==k).count()));

        for(Integer k:m.keySet()){
            System.out.println(k+"-->"+m.get(k));
        }
        /*
            Study IntStream. Try to figure out couple of ways to create
            a List<Integer> containing 1000 random integer values between 1-100
        
            Calculate the sum and average of integers in the list you created
            
            Create a Map<Integer,Integer> where the key is the number (1-100) and
            the value is the number of times it is found in the list you created
            earlier
        */
    }
}
