package collections;

public class Person implements Comparable{
    private String name="No name";
    private int age=0;

    public Person(String n, int a){
        setName(n);
        setAge(a);
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        if (name==null) return;
        if (name.length()==0) return;
        this.name = name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        System.out.println(name+" in thread "+Thread.currentThread().hashCode());
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        if ((age>=0) && (age<100)) this.age = age;
    }
    
    public boolean equals(Object o){
        if (o==null) return false;
        if (!(o instanceof Person)) return false;
        Person p=(Person)o;
        if (!this.name.equals(p.name)) return false;
        if (this.age!=p.age) return false;
        return true;
    }
    
    public int hashCode(){
        return name.hashCode();
    }

    @Override
    public int compareTo(Object o) {
        Person p=(Person)o;
        if (this.name.equals(p.name)) return this.age-p.age;
        return name.compareTo(p.name);
    }
}
