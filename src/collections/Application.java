package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Application {
    static public void main(String[] args){
        Map<Integer,String> mp=new HashMap();
        mp.put(4, "Moi");
        mp.put(8, "Hei");
        
        String s=mp.get(4);
        System.out.println(s);
        for(Integer i:mp.keySet()){
            System.out.println(i+":"+mp.get(i));
        }
        
        Map<String,Person> ms=new HashMap();
        ms.put("120367-199J", new Person("Tom",12));
        ms.put("190767-199J", new Person("Tim",11));
        System.out.println(ms.get("120367-199J").getName());
        
        ms.remove("120367-199J");
        for(String sk:ms.keySet())
            System.out.println(ms.get(sk).getName());
    }
    
    static public void xmain(String[] args){
        String[] weekdays={"monday","tuesday","wednesday",
                    "thursday","friday","saturday","sunday",
                     "Monday"};
        //for(String s:weekdays) System.out.println(s);
        Collection<String> col=Arrays.asList(weekdays);
        //for(String s:col) System.out.println(s);
        Iterator<String> iter=col.iterator();
        
        List<String> wdc=new ArrayList();
        
        while(iter.hasNext()){
            String s=iter.next();
            //System.out.println(s);
            wdc.add(s.toUpperCase());
        }
        //Collections.sort(wdc,(a,b) -> a.length()-b.length());
        wdc.sort((a,b) -> a.length()-b.length());
        System.out.println(String.join(" - ", wdc));
        
        Set<Person> ps=new TreeSet();
        ps.add(new Person("Tuomas",21));
        ps.add(new Person("Aapo",18));
        ps.add(new Person("Tuomas",21));
        for(Person p: ps)
            System.out.println(p.getName()+", "+p.getAge());
        /*
            - Create an array of weekdays
            - Use Arrays.asList to get a Collection-reference to the contents
            - Iterate the weekdays with the iterator to display the weekdays
            - Test
            - Before iteration create a new HashSet
            - On iteration add weekdays into the HashSet capitalized
            - Test
            - Add some duplicates to the original array
            - Test
        */
    }
    
    static public void ex2(String[] args){
        /* 
            - Again use a List of weekdays
            - Sort the list to alphabetical order
            - Sort the list to ascending order based on length of the string
            - Create TreeSet of weekdays and loop through it. What do you notice?
            - Create TreeSet of Persons, what do you notice? Duplicates? Order?
            - Create List<Person>. Sort it based on the ages of persons
            - Create HashMap<String,Person>, experiment
        */
    }
    
    
}
