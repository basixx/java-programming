/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daofactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jyrki
 */
public abstract class DAOBase<T> {

    abstract public void create(T vo);
    abstract public T get(int id);
    abstract public void update(T vo);
    abstract public void delete(int id);
    abstract public List<T> getAll();


    protected T get(Class<T> c,int id){
        Table t=(Table)c.getAnnotation(Table.class);
        String tn=t.name();
        try{
            T vo=c.newInstance();
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement(
                    "SELECT * FROM "+tn+" WHERE id=?");
            stm.setInt(1, id);
            ResultSet rs=stm.executeQuery();
            if (rs.next())
                populate(vo,rs);
            con.close();
            return vo;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    
    protected List<T> getAll(Class<T> c,String extra){
        //Class c=vo.getClass();
        Table t=(Table)c.getAnnotation(Table.class);
        String tn=t.name();
        ArrayList<T> al=new ArrayList();
        try{
            Connection con=getConnection();
            Statement stm=con.createStatement();
            ResultSet rs=stm.executeQuery("SELECT * FROM "+tn+" "+extra);
            while (rs.next()){
                T vo=c.newInstance();
                populate(vo,rs);
                al.add(vo);
                
            }
            con.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return al;
    }

    protected void delete(Class<T> c,int id){
        Table t=(Table)c.getAnnotation(Table.class);
        String tn=t.name();
        try{
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement("DELETE FROM "+tn+" WHERE id=?");
            stm.setInt(1, id);
            stm.execute();
            con.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    protected void populate(T vo,ResultSet rs)
    {
        Class c=vo.getClass();
        Method[] ma=c.getDeclaredMethods();
        for(Method m : ma){
            if (!m.getName().startsWith("set")) continue;
            Field f=m.getAnnotation(Field.class);
            if (f==null) continue;
            try{
                Object o=rs.getObject(f.column());
                m.invoke(vo, o);
            }
            catch(Exception ex){
                System.out.println("Failed to get column data: "+f.column());
            }
        }
    }

    protected void genericUpdate(T vo)
    {
        try{
            Table t=vo.getClass().getAnnotation(Table.class);
            String tn=t.name();
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement(
                    "SELECT * FROM "+tn+" WHERE id=?",ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);
            Method getId=vo.getClass().getMethod("getId");
            stm.setInt(1,(Integer)getId.invoke(vo));
            ResultSet rs=stm.executeQuery();
            if (rs.next()){
                for(Method m: vo.getClass().getMethods()){
                    if (!m.getName().startsWith("get")) continue;
                    if (m.getName().equals("getId")) continue;
                    Field f=m.getAnnotation(Field.class);
                    if (f==null) continue;
                    rs.updateObject(f.column(), m.invoke(vo));
                }
                rs.updateRow();
                rs.close();
            }
            con.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    protected Connection getConnection()
    {
        try{
            Class.forName(DAOFactory.driver);
            return DriverManager.getConnection(DAOFactory.connectionString+DAOFactory.dbName,DAOFactory.userName,DAOFactory.password);
        }
        catch(ClassNotFoundException cnfe){
            System.out.println("Driver "+DAOFactory.driver+" not found!");
        }
        catch(SQLException se){
            System.out.println("Connection failed!");
            System.out.println(se.getMessage());
        }
        return null;
    }
    
    
    
}
