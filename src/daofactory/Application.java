package daofactory;

import java.util.List;

public class Application {
    static public void main(String[] args){
        PersonDAO pd=DAOFactory.personDAO();
        List<Person> pl=pd.getAll();
        for(Person p:pl){
            System.out.println(p.getId()+","+p.getName()+","+p.getEmail()+","+p.getContactDate());
        }

        CompanyDAO pc=DAOFactory.companyDAO();
        List<Company> cl=pc.getAll();
        for(Company c:cl){
            System.out.println(c.getId()+","+c.getName()+","+c.getAddress());
        }
        
    }
    
}
