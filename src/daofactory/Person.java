package daofactory;

import daofactory.Field;
import daofactory.Table;
import java.util.Date;

/**
 *
 * @author Jyrki
 */
@Table(name="Person")
public class Person {
    private int id;
    private int companyId;
    private String name;
    private String email;
    private Date contactDate;

    @Field(column="id")
    public int getId() {
        return id;
    }

    @Field(column="id")
    public void setId(int id) {
        this.id = id;
    }

    @Field(column="company_id")
    public int getCompanyId() {
        return companyId;
    }

    @Field(column="company_id")
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Field(column="name")
    public String getName() {
        return name;
    }

    @Field(column="name")
    public void setName(String name) {
        this.name = name;
    }

    @Field(column="email")
    public String getEmail() {
        return email;
    }

    @Field(column="email")
    public void setEmail(String email) {
        this.email = email;
    }
 
    @Field(column="contact_date")
    public Date getContactDate() {
        return contactDate;
    }

    @Field(column="contact_date")
    public void setContactDate(Date contactDate) {
        this.contactDate = contactDate;
    }
}