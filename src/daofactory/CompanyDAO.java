package daofactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class CompanyDAO extends DAOBase<Company>{
    
    /*
        create and update are implemented demonstrating different approaches in DerbyCompanyDAO and MySqlCompanyDAO
    */

    public Company get(int id){
        Company c=get(Company.class,id);
        return c;
    }
    
    public void delete(int pk){
        delete(Company.class,pk);
    }
    
    public List<Company> getAll()
    {
        return getAll(Company.class,"ORDER BY name");
    }
}
