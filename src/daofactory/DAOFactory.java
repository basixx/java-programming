/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daofactory;


/**
 *
 * @author Jyrki
 */
public class DAOFactory {
    public static String driver="org.apache.derby.jdbc.ClientDriver"; 
    public static String connectionString="jdbc:derby://localhost:1527/";
    public static String dbName="crm";
    public static String userName="APP";
    public static String password="app";
    
    static public CompanyDAO companyDAO()
    {
        /* There are driver-dependant variations of the CompanyDAO */
        if (driver.equals("org.apache.derby.jdbc.ClientDriver")) 
                return new DerbyCompanyDAO();
        return new MySqlCompanyDAO();
    }

    static public PersonDAO personDAO(){
        /* There is just one PersonDAO, no driver-specific issues here */
        return new PersonDAO();
    }
    
}
