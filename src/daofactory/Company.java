package daofactory;

import daofactory.Table;
import daofactory.Field;


@Table(name="Company")
public class Company {
    private int id;
    private String name;
    private String address;

    @Field(column="id")
    public int getId() {
        return id;
    }

    @Field(column="id")
    public void setId(int id) {
        this.id = id;
    }

    @Field(column="name")
    public String getName() {
        return name;
    } 

    @Field(column="name")
    public void setName(String name) {
        this.name = name;
    }

    @Field(column="address") 
    public String getAddress() {
        return address;
    }

    @Field(column="address")
    public void setAddress(String address) {
        this.address = address;
    }
    
}
