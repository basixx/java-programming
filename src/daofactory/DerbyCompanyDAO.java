/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daofactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Jyrki
 */
public class DerbyCompanyDAO extends CompanyDAO{

    public void create(Company c)
    {
        try{
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement(
                    "INSERT INTO Company(name,address) VALUES(?,?)",Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, c.getName());
            stm.setString(2, c.getAddress());
            stm.executeUpdate();
            ResultSet rs=stm.getGeneratedKeys();
            if (rs!=null){
                if(rs.next())
                    c.setId(rs.getInt(1));
            }
            con.close();
        }
        catch(SQLException ex){
            ex.printStackTrace();
        }
    }
    
    public void update(Company c){
        try{
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement("SELECT * FROM Company WHERE id=?",ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);
            stm.setInt(1, c.getId());
            ResultSet rs=stm.executeQuery();
            if (rs.next()){
                rs.updateString("name",c.getName());
                rs.updateString("address",c.getAddress());
                rs.updateRow();
                rs.close();
            }
            con.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
