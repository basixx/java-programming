/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daofactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class MySqlCompanyDAO extends CompanyDAO {

    public void create(Company c)
    {
        try{
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement(
                    "INSERT INTO Company VALUES(NULL,?,?)",Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, c.getName());
            stm.setString(2, c.getAddress());
            stm.executeUpdate();
            ResultSet rs=stm.getGeneratedKeys();
            if (rs!=null){
                if(rs.next())
                    c.setId(rs.getInt(1));
            }
            con.close();
        }
        catch(SQLException ex){
            ex.printStackTrace();
        }
    }
    
    
    public void update(Company c){
        try{
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement("UPDATE Company set name=?, address=? WHERE id=?");
            stm.setString(1, c.getName());
            stm.setString(2, c.getAddress());
            stm.setInt(3, c.getId());
            stm.executeUpdate();
            con.close();
        }
        catch(SQLException ex){
            ex.printStackTrace();
        }
    }
    
}
