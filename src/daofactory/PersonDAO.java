/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daofactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author Jyrki
 */
public class PersonDAO extends DAOBase<Person>{

    public void create(Person p) {
        try{
            Connection con=getConnection();
            PreparedStatement stm=con.prepareStatement("INSERT INTO Person(name,email,contact_date) VALUES(?,?,?)",Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, p.getName());
            stm.setString(2, p.getEmail());
            stm.setDate(3, new java.sql.Date(p.getContactDate().getTime()));
            stm.executeUpdate();
            ResultSet rs=stm.getGeneratedKeys();
            if (rs!=null){
                if(rs.next())
                    p.setId(rs.getInt(1));
            }
            con.close();
        }
        catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public Person get(int id) {
        return get(Person.class, id);
    }

    @Override
    public void update(Person vo) {
        genericUpdate(vo);
    }

    @Override
    public void delete(int id) {
        delete(Person.class,id);
    }

    @Override
    public List<Person> getAll() {
        return getAll(Person.class,"ORDER BY name");
    }
    
}
