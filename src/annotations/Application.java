package annotations;

import java.lang.reflect.Method;

/*
    Create reporting annotation that holds String title().
*/

public class Application {
    static public void doReport(Object o){
        Class t=o.getClass();
        Reporting ra=(Reporting)t.getAnnotation(Reporting.class);
        if (ra==null){
            System.out.println("Not reportable "+o);
            return ;
        }
        System.out.println(ra.title());
        for(Method m:t.getMethods()){
            if (!m.getName().startsWith("get")) continue;
            ra=(Reporting)m.getAnnotation(Reporting.class);
            if (ra==null) continue;
            try{
                Object val=m.invoke(o);
                System.out.println(ra.title()+":"+val);
            }
            catch(Exception ex){
                System.out.println("Failed");
            }
        }
        /*
            Test if the object is annotated with Reporting annotation,
            if not display error and return
            Display title of reporting-annotation
            Loop through the methods of the object
            - Only process getters
            - Ignore getter if it is not annotated with Reporting annotation
            - Display title and value returned by the getter
        */
    }
    
    static public void main(String[] args){
        Person p=new Person("Mike",21);
        doReport(p);
        
        Company c=new Company("Coders Unlimited","Codealley 6","info@coders.net");
        doReport(c);
    }
    
}
