package annotations;

@Reporting(title="Yritys")
public class Company {
    private String name;
    private String address;
    private String email;
    
    public Company(String n, String a, String e){
        setName(n);
        setAddress(a);
        setEmail(e);
    }

    /**
     * @return the name
     */
    @Reporting(title="Yritys")
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the email
     */
    @Reporting(title="Sposti")
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
