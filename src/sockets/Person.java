package sockets;

public class Person {
    private String name="No name";
    private int age=0;

    public Person(String n, int a){
        setName(n);
        setAge(a);
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        if (name==null) return;
        if (name.length()==0) return;
        this.name = name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        if ((age>=0) && (age<100)) this.age = age;
    }
}
