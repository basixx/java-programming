package threads;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Concurrent {
    static Integer producerThread(){
        System.out.println("Producer calculates");
        try{
            Thread.sleep(1000);
        }
        catch(Exception x){}
        System.out.println("Producer ends");
        return 1;
    }
    
    static public void main(String[] args){
        CompletableFuture
                .supplyAsync(Concurrent::producerThread)
                .thenAccept(i -> System.out.println("Consumer "+i));

        System.out.println("Continues.....");
        
        Supplier<Integer> task1 = () -> 7;
        Supplier<Integer> task2 = () -> {
           throw new RuntimeException();    
        };
        Supplier<Integer> task3 = () -> {
           return 3;               
        };
        
        Consumer<Integer> task4 = a -> System.out.println("Consuming "+a);
        
        Function<Integer,Integer> task5= a -> a*2;

        CompletableFuture<Integer> cf=CompletableFuture
                .supplyAsync(task1);
        try{
            Thread.sleep(1000);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        cf.thenAccept(a -> System.out.println("Then "+a));
        
        CompletableFuture.supplyAsync(task1)
                .thenApply(task5)
                .thenAccept(a -> System.out.println("Second "+a));
        
        CompletableFuture.supplyAsync(task1) 
            .applyToEither(CompletableFuture.supplyAsync(task2),
                    (Integer i) -> i)
            .thenCombine(CompletableFuture.supplyAsync(task3),Integer::sum)            
            .thenAccept(System.out::println); 
    }
    
}
