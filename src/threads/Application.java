package threads;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class Point{
    volatile private int x,y;
    
    synchronized public void set(int val){
        x=val;
        try{
            Thread.sleep(10);
        }
        catch(Exception ex){}
        y=val;
    }
    
    synchronized public String toString(){
        return "("+x+","+y+")";
    }
}


public class Application implements Runnable{
    static Point pt=new Point();
    private int id;
    static Object someLock=new Object();
    static Object someEvent=new Object();
    
    public Application(int id){
        this.id=id;
        Thread t=new Thread(this);
        t.start();
    }

    public void run(){
        for(int i=0;i<100;i++){
            synchronized(someEvent){
                try{
                    someEvent.wait();
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            pt.set(id);
            try{
                Thread.sleep(30);
            }
            catch(Exception ex){}
            System.out.println("Thread "+id+", "+pt);
        }
    }

    /*
    public void run(){
        for(int i=0;i<100;i++){
            synchronized(someLock){
                pt.set(id);
                try{
                    Thread.sleep(30);
                }
                catch(Exception ex){}
                System.out.println("Thread "+id+", "+pt);
            }
        }
    }
    */
    
    static public void ex1(String[] args){
        new Application(1);
        new Application(2);
        new Application(3);
        
        Scanner c=new Scanner(System.in);
        while(true){
            String s=c.nextLine();
            synchronized(someEvent){
                someEvent.notifyAll();
            }
        }
    }
    
    static public Point create(int v){
        Point p=new Point();
        try{
            Thread.sleep(5000);
        }
        catch(Exception ex){
            
        }
        p.set(v);
        return p;
    }
    
    static public void main(String[] args){
        ScheduledExecutorService ses=Executors.newScheduledThreadPool(1);
        ses.schedule(() -> System.out.println("Scheduled"),
                3,TimeUnit.SECONDS);
        ses.shutdown();
        
        ExecutorService es = Executors.newFixedThreadPool(1);
        Future<Point> fpt=es.submit(() -> create(8));
        es.shutdown();
        try{
            System.out.println(fpt.get());
        } catch(Exception ex){}
        /*
            Create a method that return a value (Point object) after a brief wait.
            Try to start the method asynchronously with ExecutorService.submit
            and display the return value when it is available
        
            Create a method that just prints something to console. 
            Use ScheduledExecutorService to call the method with 5 second delay.
        */
    }
}
