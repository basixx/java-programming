/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jyrki
 */
public class ThreadDemo extends Thread {
    
    ThreadDemo(){
    }
    
    public void run(){
        for(int i=0;i<100;i++){
            System.out.println("Thread "+i+", "+Thread.currentThread().hashCode());
            try{
                Thread.sleep(10);
            }
            catch(Exception ex){
                
            }
        }
    }

    static void tervehdi(String s, int n){
        for(int i=0;i<n;i++) System.out.println(s);
    }
    
    static public void main(String[] args){
        new Thread(() -> tervehdi("Terve vaan",4)).start();
        /*
        ExecutorService es = Executors.newFixedThreadPool(3);
        es.execute(new ThreadDemo());
        es.execute(new ThreadDemo());
        es.execute(new ThreadDemo());
        es.execute(new ThreadDemo());
        es.execute(new ThreadDemo());
        es.shutdown();
        System.out.println("Maini käynnistyy");
        ThreadDemo td=new ThreadDemo();
        //td.start();
        for(int i=0;i<10;i++) {
            System.out.println("Main "+i);
            try{
                Thread.sleep(10);
            }
            catch(Exception ex){
                
            }
        }
    */
    }
    
}
