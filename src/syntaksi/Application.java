/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaksi;

import java.util.Scanner;
import static java.lang.Math.*;

/**
 *
 * @author jyrki
 */
public class Application {

    public static void main(String[] args){
        int value=(int)(random()*100+1);
        Scanner sc=new Scanner(System.in);
        int arvauksia=0;
        
        while(true){
            System.out.print("Anna luku 1-100:");
            String s=sc.next();
            int arvaus=0;
            try{
                arvaus=Integer.parseInt(s);
            }
            catch(NumberFormatException ex){
                System.out.println("Annoit virheellisen syötteen");
                continue;
            }
            if ((arvaus<1) || (arvaus>100)) continue;
            arvauksia++;
            if (arvaus==value) break;
            if (arvaus<value) 
                System.out.println("Liian pieni");
            else
                System.out.println("Liian iso");
        }
        //System.out.println("Onnistuit "+arvauksia+" arvauksella");
        System.out.println(String.format("Onnistuit %d arvauksella", arvauksia));
    }
    
    public static void demomain(String[] args) {
        int arvo=4;
        double desimaali=3.14;
        
        long iso=43432434534535324L;
        arvo=(int)iso;   
        
        float pii=3.14f;
        pii=(float)desimaali;
        
        String s="Moikka";
        s=s+" vaan";
        
        arvo=Integer.parseInt("123");
        
        System.out.println("Terve maailma "+arvo);
    }
    
}
