/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

/**
 *
 * @author jyrki
 */
public class Encyclopedia extends Book{
    private int wordsExplained=0;

    public Encyclopedia(String t, String a, int we){
        super(t,a);
        setWordsExplained(we);
    }
    
    @Override
    public String toString(){
        return getTitle()+" explains "+wordsExplained+" words";
    }
    
    /**
     * @return the wordsExplained
     */
    public int getWordsExplained() {
        return wordsExplained;
    }

    /**
     * @param wordsExplained the wordsExplained to set
     */
    public void setWordsExplained(int wordsExplained) {
        this.wordsExplained = wordsExplained;
    }
    
}
