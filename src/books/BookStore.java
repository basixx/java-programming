/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author jyrki
 */
public class BookStore {
    private List<Book> books=new ArrayList();

    public BookStore(){
        books.add(new Book("Hobbit","Tolkien",10.50,1));
        books.add(new Book("Player Piano","Vonnegut",14.50,5));
        books.add(new Book("Catcher in the Rye","Salinger",12.50,3));
    }
    
    public void add(Book b){
        books.add(b);
    }
    
    public void sortByTitle(){
        Collections.sort(books);
    }
    
    public void sortByAuthor(){
        books.sort((a,b) -> a.getAuthor().compareTo(b.getAuthor()));
    }
    
    public void sort(Comparator<Book> bc){
        books.sort(bc);
    }
    
    public void sellx(String title, Customer c){
        for(Book b:books){
            if(b.getTitle().equals(title)){
                c.buyBook(b);
                b.sellCopies(1);
                if (b.getNumCopies()==0){
                    books.remove(b);
                    break;
                }
            }
        }
    }
    
    public void sell(String title, Customer c){
        Iterator<Book> bi=books.iterator();
        while(bi.hasNext()){
            Book b=bi.next();
            if(b.getTitle().equals(title)){
                c.buyBook(b);
                b.sellCopies(1);
                LocalDateTime dt=LocalDateTime.now();
                DateTimeFormatter df=DateTimeFormatter
                        .ofLocalizedDateTime(FormatStyle.SHORT);
                System.out.println("Kirja ostettu "+dt.format(df));
                if (b.getNumCopies()==0){
                    bi.remove();
                }
            }
        }
    }
     
    public void showInventory(){
        System.out.println("Inventory");
        for(Book b:books){
            System.out.println(b);
        }
    }
    
    public void showInventory(Comparator<Book> bc){
        System.out.println("Inventory (sorted)");
        books.stream().sorted(bc).forEach(System.out::println);
    }
    
    public Map<String,Book> getMap(){
       return books.stream().collect(Collectors.toMap(
               b -> b.getTitle(), 
               b -> b));
    }
    
}
