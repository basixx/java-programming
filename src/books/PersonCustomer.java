/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

/**
 *
 * @author jyrki
 */
@Buyer(buyMethod="buyBook")
public class PersonCustomer implements Customer{
    private String name;
     
    public PersonCustomer(String name){
        this.name=name;
    }

    @Override
    public void buyBook(Book b) {
        System.out.println(name+" buys "+b.getTitle()+" at "+b.getPrice()*1.1+"EUR");
    }
}
