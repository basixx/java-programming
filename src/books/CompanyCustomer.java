/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

/**
 *
 * @author jyrki
 */
public class CompanyCustomer implements Customer{
    private String name;
    
    public CompanyCustomer(String name){
        this.name=name;
    }

    @Override
    public double getDiscount(){
        return 0.05;
    }
    
    @Override
    public void buyBook(Book b) {
        String s=name+" orders "+b.getTitle()+", invoice "+b.getPrice()+" +VAT "+b.getPrice()*0.1;
        System.out.println(s);
    }
    
    
}
