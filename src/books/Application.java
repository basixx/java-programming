/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;

/**
 *
 * @author jyrki
 */
public class Application {
    
    static public void main(String[] args){
        BookStore bs=new BookStore();
        Map<String,Book> bm=bs.getMap();
        bs.sortByTitle();
        bs.showInventory((a,b) -> a.getNumCopies()-b.getNumCopies());
        bs.showInventory();
        bs.sortByAuthor();
        bs.showInventory();
        bs.sort((a,b) ->(int)(a.getPrice() - b.getPrice()));
        bs.showInventory();
        bs.sell("Hobbit", new PersonCustomer("Tom Jones"));
        bs.showInventory();
        
        for(String s:bm.keySet()){
            System.out.println(s+":"+bm.get(s).getPrice());
        }
    }
    
    
    private static void showBook(Book b){
        /*
        String data=String.format("%d, %s, %s, %.2f, %d", 
                b.getId(),
                b.getTitle(),b.getAuthor(),
                b.getPrice(),b.getNumCopies());
        System.out.println(data);
        */
        System.out.println(b);
    }
    
    static void bookStore(Book book,Customer c){
        System.out.println("Alennus "+c.getDiscount());
        c.buyBook(book);
    }
    
    static void annotStore(Book b, Object o){
        Class t=o.getClass();
        Buyer ba=(Buyer)t.getAnnotation(Buyer.class);
        if (ba==null) {
            System.out.println(o+" is not a buyer");
            return;
        }
        try{
            Method m=t.getMethod(ba.buyMethod(), Book.class);
            m.invoke(o, b);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    static public void xmain(String[] args){
        Book b=new Book("Hobbit","Tolkien",12.32,5);
        Object ob=new PersonCustomer("Teppo Taavitsainen");
        annotStore(b,ob);
        annotStore(b,new SomeBuyer());
        //Class c=Book.class;
        Class c=b.getClass();
        try{
            //Object o=c.newInstance();
            for(Method m:c.getMethods()){
                if (!m.getName().startsWith("get")) continue;
                Object o=m.invoke(b);
                System.out.println(o);
            }
            //System.out.println(o);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    static public void bookmain(String[] args){
        System.out.println("Kirjakauppa");
        Book b3=new Book("Catcher in the Rye","Salinger",12.50,3);
        Encyclopedia e1=new Encyclopedia("Encyclopedia Britannica","Various",21342344);
        e1.setPrice(43);
        PersonCustomer pc=new PersonCustomer("John Lennon");
        CompanyCustomer cc=new CompanyCustomer("Coders Unlimited");
        
        bookStore(b3,pc);
        bookStore(e1,cc);
        
        Customer c=new Customer(){
            @Override
            public void buyBook(Book b) {
                System.out.println("Anonyymi ostaja ostaa "+b.getTitle());
            }
        };
        bookStore(b3,c);
        bookStore(b3,new Customer(){
            @Override
            public void buyBook(Book b) {
                System.out.println("Taas menee");
            }
        });
        bookStore(b3,(b) -> System.out.println("Ostetaan "+b.getTitle()));
        
    }
}
