/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

import java.time.Month;
import java.time.YearMonth;
import java.util.Scanner;

/**
 *
 * @author jyrki
 */
public class Book implements Comparable<Book>{
    private int id;
    private String title="";
    private String author="";
    private double price=0;
    private int numCopies=0;
    private static int nextId=1;
    private YearMonth published=YearMonth.of(
            1900+(int)(Math.random()*110),
            (int)(Math.random()*12)
    );
    
    public Book(){
        this("","");
    }
    
    static public Book teeKirja(){
        Book b=new Book();
        Scanner c=new Scanner(System.in);
        System.out.print("Anna title:");
        b.setTitle(c.next());
        return b;
    }
    
    public Book(String t, String a){
        this(t,a,0,0);
    }
    
    public Book(String t, String a, double p, int nc){
        id=nextId++;
        setTitle(t);
        setAuthor(a);
        setPrice(p);
        setNumCopies(nc);
    }
    
    public int sellCopies(int nc){
        int sold=nc<=numCopies ? nc : numCopies;
        setNumCopies(numCopies-sold);
        return sold;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        if (title==null) title="";
        this.title = title;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        if (author==null) author="";
        this.author = author;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        if ((price<0) || (price>50)) return;
        this.price = price;
    }

    /**
     * @return the numCopies
     */
    public int getNumCopies() {
        return numCopies;
    }

    /**
     * @param numCopies the numCopies to set
     */
    public void setNumCopies(int numCopies) {
        if (numCopies<0) return;
        this.numCopies = numCopies;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    @Override
    public String toString(){
        String data=String.format("%d, %s, %s, %.2f, %d, %s", 
                id,
                title,this.author,
                this.price,this.numCopies,
                published.toString());
        return data;
    }
    
    @Override
    public boolean equals(Object o){
        if (o==null) return false;
        if (!(o instanceof Book)) return false;
        Book b=(Book)o;
        if (!this.title.equals(b.title)) return false;
        if (!this.author.equals(b.author)) return false;
        return true;
    }
    
    @Override
    public int hashCode(){
        return title.hashCode();
    }

    @Override
    public int compareTo(Book o) {
        if (this.title.equals(o.title))
            return author.compareTo(o.author);
        return title.compareTo(o.title);
    }
}
