/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

/**
 *
 * @author jyrki
 */
public class ComicBook extends Book{
    private String illustrator;

    public ComicBook(String t, String a, String i){
        super(t,a);
        this.setIllustrator(i);
    }
    
    @Override
    public String toString(){
        return super.toString()+", illustrator "+illustrator;
    }
    
    /**
     * @return the illustrator
     */
    public String getIllustrator() {
        return illustrator;
    }

    /**
     * @param illustrator the illustrator to set
     */
    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }
    
}
