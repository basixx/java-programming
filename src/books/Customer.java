/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package books;

/**
 *
 * @author jyrki
 */

@FunctionalInterface
public interface Customer {
    public void buyBook(Book b);
    //void jotain();
    
    default public double getDiscount(){
        return 0;
    }
}
